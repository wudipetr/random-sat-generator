# Random SAT generator
Randomly generates instances of weighted SAT problem.

## Usage

To compile the program just run `mvn package`. Then JAR file will be generated in the "target" directory.

Possible arguments (all of them must be followed by the desired value):
* `-c` or `--clauses` number of clauses in the output formula (default: 10)
* `-l` or `--literals` number of literals in each clause (default: 3)
* `-n` or `--repeats` number of SAT problem instances to be generated (default: 1)
* `-v` or `--variables` number of variables which can appear in the formula (default: 5)

## Output
The program generates `n` lines, each containing one SAT problem instance.

The line consist of two parts: first one specifying variables and the second one specifying actual formula.

Example output:

`4 A 143 B 395 C 456 D 288 +A-B-D -A-B-C -B-A+A -D-B-C -B-C+D`

First number of the line specifies number of variables.

It's followed by variable names (eg. A) and their weights (eg. 143).

Last part of the output is actual formula. The example above contains formula:

`+A-B-D -A-B-C -B-A+A -D-B-C -B-C+D`

There is `c` clauses in the formula (eg. +A-B-D) delimited by space.

Clauses consists of literals (eg. +A or -B). Each literal contains + or - sign at the beginning and a variable name 
after it. The variable name could be string containing one letter (A-Z, both uppercase or lowercase) or more letters if 
necessary. The + or - sign means that the variable in the literal is negated (-) or that it's not (+).  