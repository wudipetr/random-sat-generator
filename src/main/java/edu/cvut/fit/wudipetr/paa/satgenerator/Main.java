package edu.cvut.fit.wudipetr.paa.satgenerator;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    private static final char[] POSSIBLE_VARIABLE_NAMES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    private static final String ARG_VARIABLES = "v";
    private static final String ARG_VARIABLES_LONG = "variables";
    private static final String ARG_VARIABLES_DESCRIPTION = "number of variables in the formula";
    private static final String ARG_CLAUSES = "c";
    private static final String ARG_CLAUSES_LONG = "clauses";
    private static final String ARG_CLAUSES_DESCRIPTION = "number of clauses in the formula";
    private static final String ARG_LITERALS = "l";
    private static final String ARG_LITERALS_LONG = "literals";
    private static final String ARG_LITERALS_DESCRIPTION = "number of literals in each clause (default: 3)";
    private static final String ARG_REPEATS = "n";
    private static final String ARG_REPEATS_LONG = "repeats";
    private static final String ARG_REPEATS_DESCRIPTION = "number of SAT problem instances to be generated";
    private static final int DEFAULT_VARIABLE_COUNT = 5;
    private static final int DEFAULT_CLAUSE_COUNT = 10;
    private static final int DEFAULT_LITERAL_COUNT = 3;
    private static final int MAX_VARIABLE_WEIGHT = 500;

    /**
     * Generates options of program's input parameters
     * @return apache cli options
     */
    private static Options generateInputOptions() {
        Options options = new Options();
        Option variables = new Option(ARG_VARIABLES, ARG_VARIABLES_LONG, true, ARG_VARIABLES_DESCRIPTION);
        Option clauses = new Option(ARG_CLAUSES, ARG_CLAUSES_LONG, true, ARG_CLAUSES_DESCRIPTION);
        Option literals = new Option(ARG_LITERALS, ARG_LITERALS_LONG, true, ARG_LITERALS_DESCRIPTION);
        Option repeats = new Option(ARG_REPEATS, ARG_REPEATS_LONG, true, ARG_REPEATS_DESCRIPTION);
        options.addOption(variables);
        options.addOption(clauses);
        options.addOption(literals);
        options.addOption(repeats);
        return options;
    }

    /**
     * Loads specified parameter from program's arguments in integer format
     * @param cmd apache-cli command line that might contain the param
     * @param paramName identifier of the parameter
     * @param defaultValue param's default value in case the it's not present in input params
     * @return integer value of specified parameter
     */
    private static int parseIntParam(CommandLine cmd, String paramName, int defaultValue) throws NumberFormatException {
        int result = defaultValue;
        String resStr = cmd.getOptionValue(paramName);
        if(resStr != null) {
            result = Integer.parseInt(resStr);
        }
        return result;
    }

    public static void main(String[] args) {
        Options options = generateInputOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        }
        catch(ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("random-sat-generator", options);
            System.exit(1);
            return;
        }
        int variableCount;
        int clauseCount;
        int literalCount;
        int repeatCount;
        try {
            variableCount = parseIntParam(cmd, ARG_VARIABLES, DEFAULT_VARIABLE_COUNT);
            clauseCount = parseIntParam(cmd, ARG_CLAUSES, DEFAULT_CLAUSE_COUNT);
            literalCount = parseIntParam(cmd, ARG_LITERALS, DEFAULT_LITERAL_COUNT);
            repeatCount = parseIntParam(cmd, ARG_REPEATS, 1);
        }
        catch(NumberFormatException e) {
            System.err.println("Error parsing input parameters");
            return;
        }
        for(int i = 0; i < repeatCount; i++) {
            generateSAT(variableCount, clauseCount, literalCount);
        }
    }

    /**
     * Generates and prints instance of SAT weighted problem (a formula) to standard output. The output contains
     * variables with weights associated to them and actual formula.
     * @param variableCount number of variables which can become part of literals
     * @param clauseCount number of clauses in the formula
     * @param literalCount number of literals in each clause
     */
    private static void generateSAT(int variableCount, int clauseCount, int literalCount) {
        List<String> variables = generateAndPrintVariables(variableCount);
        generateAndPrintSAT(variables, clauseCount, literalCount);
    }

    /**
     * Generates list of variables with weights associated to them and prints it to the standard output
     * @param variableCount number of variables to be generated
     * @return list of variable names (weights are print to stdout and forgotten)
     */
    private static List<String> generateAndPrintVariables(int variableCount) {
        List<String> variableNames = new ArrayList<>(variableCount);
        Random random = new Random();
        System.out.format("%d", variableCount);
        for(int i = 0; i < variableCount; i++) {
            String variableName = mkVariableName(i);
            int variableWeight = random.nextInt(MAX_VARIABLE_WEIGHT);
            variableNames.add(variableName);
            System.out.format(" %s% d", variableName, variableWeight);
        }
        return variableNames;
    }

    /**
     * Generates a variable name based on input parameter. It always returns same name for same inputs and the output
     * is unique for different inputs. The output can contain letters A-Z both upper and lowercase.
     * @param i unique identifier of the variable
     * @return variable name
     */
    private static String mkVariableName(int i) {
        int divisor = POSSIBLE_VARIABLE_NAMES.length;
        StringBuilder sb = new StringBuilder();
        while(i >= 0) {
            sb.append(POSSIBLE_VARIABLE_NAMES[i % divisor]);
            i /= divisor;
            i--;
        }
        return sb.toString();
    }

    /**
     * Generates a formula (instance of SAT problem) and prints it to standard output
     * @param variables list of variables that can be used in the formula construction
     * @param clauseCount number of clauses in the formula
     * @param literalCount number of literals in each clause
     */
    private static void generateAndPrintSAT(List<String> variables, int clauseCount, int literalCount) {
        int numberOfVariables = variables.size();
        Random random = new Random();
        for(int i = 0; i < clauseCount; i++) {
            System.out.append(' ');
            boolean[] usedVariables = new boolean[numberOfVariables];
            for(int j = 0; j < literalCount; j++) {
                char sign = random.nextBoolean() ? '+' : '-';
                int variableIndex = random.nextInt(numberOfVariables);
                while(usedVariables[variableIndex]) {
                    variableIndex = random.nextInt(numberOfVariables);
                }
                usedVariables[variableIndex] = true;
                String variable = variables.get(variableIndex);
                System.out.format("%c%s", sign, variable);
            }
        }
        System.out.append('\n');
    }
}
